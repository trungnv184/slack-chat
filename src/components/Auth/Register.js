import React, { useState } from "react";
import {
  Grid,
  Form,
  Segment,
  Button,
  Message,
  Header,
  Icon,
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import firebase from "../firebase";
import * as constants from "../../constants/Constants";
import md5 from "md5";

const Register = (props) => {
  const controls = {
    email: "email",
    username: "username",
    password: "password",
    passwordConfirmation: "passwordConfirmation",
  };

  const [userName, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirmation, setPasswordConfirmation] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [errors, setErrors] = useState([]);
  const [firebaseRef] = useState(firebase.database().ref("users"));

  const handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;

    switch (name) {
      case controls.username: {
        setUserName(value);
        break;
      }

      case controls.email: {
        setEmail(value);
        break;
      }

      case controls.password: {
        setPassword(value);
        break;
      }

      case controls.passwordConfirmation: {
        setPasswordConfirmation(value);
        break;
      }

      default:
        break;
    }
  };

  const createNewUser = async (email, password) => {
    try {
      setIsLoading(true);

      const newUser = await firebase
        .auth()
        .createUserWithEmailAndPassword(email, password);

      if (!newUser) {
        return;
      }

      await newUser.user.updateProfile({
        displayName: userName,
        photoURL: `http://gravatar.com/avatar/${md5(
          newUser.user.email
        )}?d=identicon`,
      });

      firebaseRef.child(newUser.user.uid).set({
        name: newUser.user.displayName,
        avatar: newUser.user.photoURL,
      });

      console.log(newUser);
    } catch (error) {
      setErrors(
        [].concat({
          message: error.message,
        })
      );
    } finally {
      setIsLoading(false);
    }
  };

  const isPasswordValid = () => {
    return !(
      password.length < constants.PASSWORD_MIN_LENGTH ||
      passwordConfirmation.length < constants.PASSWORD_MIN_LENGTH ||
      passwordConfirmation !== password
    );
  };
  const isFormEmpty = () => {
    return (
      !email.length ||
      !userName.length ||
      !password.length ||
      !passwordConfirmation.length
    );
  };

  const displayErrors = () => {
    if (errors.length === 0) {
      return;
    }

    return errors.map((error, index) => <p key={index}>{error.message}</p>);
  };

  const handleSubmitionForm = async () => {
    if (isFormEmpty()) {
      setErrors(
        [].concat({
          message: "Fill in all fields",
        })
      );

      return;
    }

    if (!isPasswordValid()) {
      setErrors(
        [].concat({
          message: "Password invalid",
        })
      );
      return;
    }

    await createNewUser(email, password);
  };

  return (
    <Grid textAlign="center" verticalAlign="middle" className="app">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h2" icon color="orange" textAlign="center">
          <Icon name="puzzle piece" color="orange" />
          Register for DevChat
        </Header>

        <Form size="large" onSubmit={handleSubmitionForm}>
          <Segment stacked padded clearing>
            <Form.Input
              fluid
              name="username"
              icon="user"
              iconPosition="left"
              placeholder="Username"
              type="text"
              onChange={(event) => handleChange(event)}
              value={userName}
            />

            <Form.Input
              fluid
              name="email"
              icon="mail"
              iconPosition="left"
              placeholder="Email"
              type="email"
              value={email}
              onChange={(event) => handleChange(event)}
            />

            <Form.Input
              fluid
              name="password"
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              type="password"
              value={password}
              onChange={(event) => handleChange(event)}
            />

            <Form.Input
              fluid
              name="passwordConfirmation"
              icon="repeat"
              iconPosition="left"
              type="password"
              value={passwordConfirmation}
              placeholder="Password Confirmation"
              onChange={(event) => handleChange(event)}
            />

            <Button
              type="submit"
              color="orange"
              fluid
              size="large"
              loading={isLoading}
            >
              Submit
            </Button>
          </Segment>
        </Form>
        {errors.length > 0 && (
          <Message error>
            <h3>Error</h3>
            {displayErrors()}
          </Message>
        )}
        <Message>
          Already a user ? <Link to="/login">Login</Link>
        </Message>
      </Grid.Column>
    </Grid>
  );
};

export default Register;
