import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory,
} from "react-router-dom";
import App from "./components/App";
import Login from "./components/Auth/Login";
import Register from "./components/Auth/Register";
import firebase from "./components/firebase";
import "semantic-ui-css/semantic.min.css";
import "./index.css";

import * as serviceWorker from "./serviceWorker";

const RootApp = () => {
  const history = useHistory();

  useEffect(() => {
    return () => {
      firebase.auth().onAuthStateChanged((user) => {
        if (!user) {
          history.push("/login");
        } else {
          history.push("/");
        }
        console.log(user);
      });
    };
  }, []);

  return (
    <Router>
      <Switch>
        <Route path="/" component={App} exact></Route>
        <Route path="/login" component={Login} exact></Route>
        <Route path="/register" component={Register} exact></Route>
      </Switch>
    </Router>
  );
};

ReactDOM.render(<RootApp />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
